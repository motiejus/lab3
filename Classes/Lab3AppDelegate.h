//
//  Lab3AppDelegate.h
//  Lab3
//
//  Created by John Williamson on 05/02/2010.
//  Copyright Computing science 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EAGLView;

@interface Lab3AppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    EAGLView *glView;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet EAGLView *glView;

@end

